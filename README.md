# DM_CW3

## 项目文件夹结构

- codes文件夹

该文件夹可以放入每个人的工作相关内容，代码等。但需要创建以自己学号为名的文件夹进行保存。

- lectures文件夹

放入了与作业相关的课程PPT

# Business Understanding

## Business Objectives：

 - final goal
 
    通过使用数据挖掘工具与方法提取出不同国家语言的方言特征并可以使用模型来识别出方言出自于哪一个国家。 

 - Background：

    总共有6个来自于6个说西班牙语国家的语料库，每个语料库都有着相同的主题。

 - My corpus characteristics
 
## Data Mining objective

- example data

    直接找出示例的几行，并分析数据的情况

- key features

    *将自己的筛选语料库的seed terms（关键词）放入到N-grams中，找出与其相搭配的词汇，并下载成.csv文件。假如无法找到，需要标注出自己如何找出新的与关键词有关的词汇（例如使用了什么方法）。

# Data Understanding

## collect initial data

根据CW0的内容自行进行一定程度的总结
    
## Describe Data

- size

    利用SketchEngine右上角搜索框右侧的i点击进行查看，内容包括Words,Sentences,Paragraphs等数据

- format
    
    主要检查文本的编码格式，如果要使用代码的话请参考"codes/sc20sd"文件

- content

    词

    句

    短语

    标题


- 数据的主题：计算机，大数据，数据，数据科学，大学，政府政策

- 主要包含了相同关键词以及根据自己的地域设定的9个关键词和5个特殊词。
    
    其中5个特殊词与前9个词也有着相关性 主要是本地国家的大学 政府政策以及有名的互联网公司。

## Explore Data

- 语料库中包含了什么高频短语:
  
    SketchEngine中的key words: MULTI-WORDS TERMS。表中各个字段的含义

    寻找与主题相关的高频词或词组


## Data issues
  
- 文本内容有不需要的字段

    例如不必要的链接，数字等等，对自己的数据文本进行一定的数据清理

- 文本内容有拼写错误

# Data Preparation

## Select data

选出通过N-grams得出所有词。是否放入词频有待考证。

## Clean data

需要检查自己的数据中是否有非法字符。

## Constract data
将.csv文件转化为.arff格式文件。
    
| Key festures | country |
| ------ | ------ |
| 关键词 | 国家顶级域名 |


## Integrate data

将所有人处理后的数据进行整合。

# Modeling

## Select Modeling

暂定选择使用决策树来进行分类。

## Test design

使用交叉验证的方法对准确率进行测试。

# Evaluation

观察测试结果，分析效果。



