# 数据处理，处理掉一些多余的字符和段落，每一行输出为一个单独的.txt文件
ms = open("./datasets/VE_Spanish_Wu.txt", encoding='UTF-8')  # 将个人的语料库名字放进去

a = 1
for line in ms.readlines():
    if line == '\n':
        line = line.strip("\n")
    line = line.replace('<p>', '')
    line = line.replace('</p>', '')
    line = line.lstrip()
    if len(line.rstrip()) > 5:
        with open(f"VE\{a}.txt", 'w', encoding='UTF-8') as mon:  # 这里的VE是你需要新建的文件夹
            mon.write(line)
        a = a + 1
