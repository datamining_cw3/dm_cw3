try:
    #result
    dataFile = open('datafile.txt', 'w', encoding='latin-1')
    #自己的语料库
    corpusFile= open('PE_spanishPeru_HU.txt', 'r', encoding='latin-1')
    #自己的语料库与其他人对比后的特征值
    hghwordsFile = open('hghwords.txt', 'r', encoding='latin-1')
    sentences = corpusFile.readlines()
    keywords =[]
    for key in hghwordsFile.readlines():
        keywords.append(key.replace("\n", ""))
    for sentence in sentences:
         count = 0
         for word in keywords:
             if word in sentence:
                 count = count + 1
         if count >= 3:
             print(sentence)
             dataFile.write(sentence)
    corpusFile.close()
    dataFile.close()
except KeyError:
    print(KeyError)


